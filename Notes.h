#pragma once

#include "DrawableGameComponent.h"

using namespace Library;

namespace Rendering
{
    class Notes : public DrawableGameComponent
    {
        
        RTTI_DECLARATIONS(Notes, DrawableGameComponent)



    public:
        Notes(Game& game, Camera& camera, std::wstring textureName, const std::wstring modelDes, int modelValue);
        ~Notes();

        virtual void Initialize() override;

        void SetPosition(const float translateX, const float translateY, const float translateZ, const float scaleFactor, const float rotateX, const float rotateY, const float rotateZ);
        virtual void Update(const GameTime& gameTime) override;
		virtual void Draw(const GameTime& gameTime) override;
        XMFLOAT4X4* WorldMatrix() { return &mWorldMatrix; }

        
        /*float Getx() 
        {
            return x;
        }
        float Gety()
        {

            return y;
        }
        float Getz()
        {
            return z;

        }*/
        void SetViewed(bool v) 
        {
            viewed = v;
        }

        bool GetViewed() 
        {
            return viewed;
        }

        /*void SetTex(std::wstring tex)
        {
            textureN = tex;
        }*/

        /*ID3D11ShaderResourceView* Tex()
        {
            return mTextureShaderResourceView;
        }*/
        const std::wstring GetModelName() { return modelDes; }
        int const ModelVal() { return mModelValue; }


    private:
       /* typedef struct _BasicEffectVertex
        {
            XMFLOAT4 Position;
            XMFLOAT4 Color;


            _BasicEffectVertex() { }

            _BasicEffectVertex(XMFLOAT4 position, XMFLOAT4 color)
                : Position(position), Color(color) { }
        } BasicEffectVertex;*/
        typedef struct _TextureMappingVertex
        {

            XMFLOAT4 Position;

            XMFLOAT2 TextureCoordinates;

            _TextureMappingVertex() { }

            _TextureMappingVertex(XMFLOAT4 position, XMFLOAT2 textureCoordinates)

                : Position(position), TextureCoordinates(textureCoordinates) { }
        } TextureMappingVertex;

        Notes();
        Notes(const Notes& rhs);
        Notes& operator=(const Notes& rhs);

        ID3DX11Effect* mEffect;
        ID3DX11EffectTechnique* mTechnique;
        ID3DX11EffectPass* mPass;
        ID3DX11EffectMatrixVariable* mWvpVariable;

        ID3D11InputLayout* mInputLayout;		
        ID3D11Buffer* mVertexBuffer;
        ID3D11Buffer* mIndexBuffer;

        ID3D11ShaderResourceView* mTextureShaderResourceView; //points to the texture map object that will be used as a shader resource in our D3D programmable rendering pipeline

        ID3DX11EffectShaderResourceVariable* mColorTextureVariable; //is used by D3D to handle the texture information from the shader file.

        XMFLOAT4X4 mWorldMatrix;
		float mAngle;
        std::wstring textureN;

        //float x = 0, y = 0, z = 0;
        std::wstring modelDes;
        int mModelValue;

        bool viewed;


    };
}
